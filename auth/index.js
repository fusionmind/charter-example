import store from '../store/store';
import axios from 'axios';

const PROXY_URL = process.env.FRONTENDPROXY_URL;

const LOGIN_PATH = '/login';
const LOGOUT_PATH = '/logout';
const VALIDATE_PATH =  '/validate';

export default {


  user: {
    authenticated: false
  },


  login(context, creds, redirect) {
    console.log(PROXY_URL);

    axios.post(PROXY_URL+LOGIN_PATH, creds).then((data) => {

      console.log('++++++proxy url++++ ' + PROXY_URL.NODE_ENV);

      localStorage.setItem('token', data.data.token)
      localStorage.setItem('userInfo', JSON.stringify(data.data.userInfo))


      this.user.authenticated = true;

      if (redirect) {
        context.$router.push(redirect);
      }
    }, (err) => {
      context.error = err;
    });
  },

  logout(context) {
    axios.get(PROXY_URL+LOGOUT_PATH, {headers: this.getAuthHeader()})
      .then((data) => {
      this.user.authenticated = false;
      localStorage.removeItem('token');
      localStorage.removeItem('userInfo');
      store.commit('setUserInfo', {});
    }, (err) => {
        context.error = err;
    });
  },

  validateAuth(context, failedRedirect) {
    var jwt = localStorage.getItem('token');
    if(jwt) {
      axios.get(PROXY_URL+VALIDATE_PATH, {headers: this.getAuthHeader()})
        .then((data) => {
          this.user.authenticated = data.data.valid;

          if (!data.data.valid) {
            localStorage.removeItem('token');
            if (failedRedirect) {
              context.$router.push(failedRedirect);
            }
          }
        }, (err) => {
          context.error = err;
        })
    } else {
      this.user.authenticated = false;
      if (failedRedirect) {
        context.$router.push(failedRedirect);
      }
    }
  },

  checkAuth() {
    var jwt = localStorage.getItem('token')
    if(jwt) {
      this.user.authenticated = true
    }
    else {
      this.user.authenticated = false
    }
  },

  checkAuth() {
    var jwt = localStorage.getItem('token');
    var user = localStorage.getItem('userInfo');


    if(jwt && user) {
      var userInfo = JSON.parse(user)
      this.user.authenticated = true
      store.commit('setUserInfo', {token: jwt, userInfo: userInfo});
    }
    else {
      this.user.authenticated = false
    }
  },

  getAuthHeader() {
    return {
      'Authorization': localStorage.getItem('token')
    }
  }
}
