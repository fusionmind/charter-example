import store from '../store/store';
import auth from '../auth';
import axios from 'axios';

const PROXY_URL = process.env.FRONTENDPROXY_URL;
export default {
  getSearchParameters(context, path, contextData) {
    let config = {
      headers: auth.getAuthHeader()
    };
    return axios.get(PROXY_URL+path, config)
      .then((data) => {
        context.$data[contextData] = data.data;
      }, (err) => {
        context.error = err;
      });
  },
  postSearch(context, path, parameters){
    let config = {
      headers: auth.getAuthHeader()
    };
    return axios.post(PROXY_URL+path, parameters, config)
      .then((data) => {
        store.commit('setResults', data.data.results);
        context.$router.push('results');
      }, (err) => {
       context.error = err;
    });
  }
}
