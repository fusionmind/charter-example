import auth from '../auth';
import axios from 'axios';

export const getAccountDetail = ({ commit }, accountId) => {
  var basicUrl = process.env.FRONTENDPROXY_URL + '/api/accounts/';
  basicUrl += accountId;
  basicUrl += '/accountdetails';
  var config = {
    headers: auth.getAuthHeader()
  };
  console.debug('getAccountDetail: basicUrl:' + basicUrl);
  axios.get(basicUrl, config).then((response) => {
    // this.currentAccount = response.data.details;
    // console.debug('currentAccount: ' + JSON.stringify(this.currentAccount));
    let account = response.data.details;
    account.id = accountId;
    console.debug('getAccountDetail | currentAccount: ' + JSON.stringify(account));
    commit('setCurrentAccount', account)
  }, (error) => {
    console.error(error);
  })
};

