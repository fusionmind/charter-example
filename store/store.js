import Vuex from 'vuex';
import Vue from 'vue';

import * as actions from './actions';

Vue.use(Vuex);

var store = new Vuex.Store({
  state: {
    userInfo: {},
    token: null,
    results: [],
    searchParams: {},
    currentAccount: {}

  },
  mutations: {
    setUserInfo(state, payload) {
      state.userInfo = payload.userInfo;
      state.token = payload.token;
    },
    setResults(state, results) {
      state.results = results;
    },
    setSearchParams(state, searchParams) {
      state.searchParams = searchParams;
    },
    setCurrentAccount(state, account) {
      console.debug('setCurrentAccount | account: ' + JSON.stringify(account));
      state.currentAccount = account;
    }
  },
  actions,
  getters: {
    currentAccount(state) {
      return state.currentAccount;
    }
  }
});

export default store
