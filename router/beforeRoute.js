import auth from '../auth'

export default {

  guardRoute (to, from, next) {
    auth.checkAuth();
    if (auth.user.authenticated) {
      next()
    } else {
      next(false)
    }
  }

}
