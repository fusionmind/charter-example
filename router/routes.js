import beforeRouter from './beforeRoute';
import Account from '../components/Account.vue';
import AccountEdit from '../components/AccountEdit.vue';
import Contact from '../components/Contact.vue';
import Login from '../components/Login.vue';
import Results from '../components/Results.vue';
import Search from '../components/Search.vue';
import TestAccounts from '../components/TestAccounts.vue';

export const routes = [
  { path: '/login', component: Login},
  { path: '/account', component: Account, beforeEnter: beforeRouter.guardRoute},
  { path: '/search', component: Search, beforeEnter: beforeRouter.guardRoute},
  { path: '/contact', component: Contact, beforeEnter: beforeRouter.guardRoute},
  { path: '/results', component: Results, beforeEnter: beforeRouter.guardRoute},
  { path: '/testaccounts', component: TestAccounts, beforeEnter: beforeRouter.guardRoute},
  { path: '/:id/edit', component: AccountEdit, name:"accountEdit", props: true, beforeEnter: beforeRouter.guardRoute},
  { path: '*', redirect: '/login' },
];
